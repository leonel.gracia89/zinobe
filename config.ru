require './app'
require_relative './config/environment'
require 'sidekiq/web'

run ApplicationController
use AuthenticationsController
use RegistrationsController
use ContactsController
use DashboardController
