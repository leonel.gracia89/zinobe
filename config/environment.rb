ENV['SINATRA_ENV'] ||= 'development'
ENV['RACK_ENV'] ||= ENV['SINATRA_ENV']

require 'bundler/setup'
Bundler.require(:default, ENV['SINATRA_ENV'])

require_all 'app/**/*.rb'
