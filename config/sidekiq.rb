require 'sinatra/activerecord'
Dir['./app/models/*.rb'].each {|file| require file }
Dir['./app/workers/*.rb'].each {|file| require file }
Dir['./app/services/*.rb'].each {|file| require file }

Sidekiq.configure_server do |config|
  config.redis = { url: 'redis://127.0.0.1:6379' }
end
