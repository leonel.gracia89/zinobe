class CreateContacts < ActiveRecord::Migration[6.0]
  def change
    create_table :contacts do |t|
      t.string :email, null: false, unique: true
      t.string :nit, null: false, unique: true
      t.string :name, null: false
      t.string :phone, null: false
      t.integer :contact_type
      t.integer :status, default: 0
      t.references :account, foreign_key: true

      t.timestamps
    end
  end
end
