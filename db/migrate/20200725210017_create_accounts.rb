class CreateAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :accounts do |t|
      t.string :email, null: false, unique: true
      t.string :nit, null: false, unique: true
      t.string :name, null: false
      t.string :password_digest

      t.timestamps
    end
  end
end
