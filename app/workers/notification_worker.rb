class NotificationWorker
  include Sidekiq::Worker

  def perform(contact_id)
    contact = Contact.find(contact_id)
    NotificationService.new(contact).notify!
  end
end
