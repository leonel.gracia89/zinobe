class ContactsController < ApplicationController

  get '/contacts/new' do
    authenticate_account!
    erb :'contacts/new', application_layout
  end

  post '/contacts' do
    authenticate_account!

    contact = Contact.new.tap do |c|
      c.nit = params['nit']
      c.name = params['name']
      c.email = params['email']
      c.phone = params['phone']
      c.account_id = @current_user.id
      c.contact_type = params['type'].to_i
    end

    if contact.save
      NotificationWorker.perform_async(contact.id)
      flash[:success] = 'Contact created'
    else
      flash[:danger] = contact.errors.full_messages
    end

    redirect :'/dashboard'
  end
end
