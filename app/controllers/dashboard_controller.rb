class DashboardController < ApplicationController

  get '/dashboard' do
    authenticate_account!
    @contacts = @current_user.contacts.search(params['search_param'])
    erb :'dashboard/index', application_layout
  end
end
