class RegistrationsController < ApplicationController

  get '/registrations/new' do
    if @current_user
      redirect :'/dashboard'
    else
      erb :'/registrations/new', application_layout
    end
  end

  post '/registrations/signup' do
    account = Account.new.tap do |a|
      a.nit = params['nit']
      a.name = params['name']
      a.email = params['email']
      a.password = params['password']
    end

    if account.save
      session['account'] = account.id
      @session = session

      redirect :'/dashboard'
    end

    flash[:danger] = account.errors.full_messages
    redirect :'/registrations/new'
  end
end
