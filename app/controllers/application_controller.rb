class ApplicationController < Sinatra::Base
  register Sinatra::Flash

  configure do
    enable :sessions
    set :views, 'app/views'
    set :session_secret, SecureRandom.hex(32)
    set :protect_from_csrf, true
  end

  before do
    response.headers["Cache-Control"] = "no-cache, no-store"
  end

  get '/' do
    if @current_user
      redirect :'/dashboard'
    else
      erb :'/authentications/new', application_layout
    end
  end

  def application_layout
    { layout: :'/layouts/application' }
  end

  def setting_session(account)
    session['account'] = account.id
    @session = session

    redirect :'/dashboard'
  end

  def authenticate_account!
    @current_user = Account.find(session["account"]) if session["account"]

    redirect :'/authentications/new' unless @current_user
  rescue ActiveRecord::RecordNotFound
    redirect :'/authentications/new'
  end
end
