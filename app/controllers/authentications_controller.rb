class AuthenticationsController < ApplicationController

  get '/authentications/new' do
    erb :'/authentications/new', application_layout
  end

  post '/authentications/login' do
    account = Account.new(email: params['email'], password: params['password'])
    authentication_service = AuthenticationService.new(account)

    setting_session(authentication_service.account) if authentication_service.valid_credentials?

    flash[:danger] = 'Invalid credentials.'
    redirect :'/authentications/new'
  end

  get '/authentications/logout' do
    session.clear
    redirect :'/'
  end
end
