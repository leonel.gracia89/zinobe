class NotificationService
  attr_writer :contact

  def initialize(contact)
    @contact = contact
  end

  def notify!
    service = nil

    case @contact.contact_type
    when 'beneficiario'
      service = PhoneNotificationService.new
    when 'padrino'
      service = EmailNotificationService.new
    end

    NotificationContactService.new(service).notify(@contact)
    @contact.notified!
  end
end
