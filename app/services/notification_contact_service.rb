class NotificationContactService
  attr_writer :notification_contact_service

  def initialize(notification_contact_service)
    @notification_contact_service = notification_contact_service
  end

  def notify(contact)
    @notification_contact_service.send_notification(contact)
  end
end
