require_relative 'abstract_notification_contact_service'
require 'net/http'

class PhoneNotificationService < AbstractNotificationContactService

  def send_notification(contact)
    uri = URI("#{ BASIC_URL }?phone=#{ contact.phone }")
    Net::HTTP.get_response(uri)
  end
end
