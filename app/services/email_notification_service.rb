require_relative 'abstract_notification_contact_service'
require 'net/http'

class EmailNotificationService < AbstractNotificationContactService

  def send_notification(contact)
    uri = URI("#{ BASIC_URL }?email=#{ contact.email }")
    Net::HTTP.get_response(uri)
  end
end
