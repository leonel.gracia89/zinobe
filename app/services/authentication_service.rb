class AuthenticationService
  attr_reader :account

  def initialize(account)
    @account = account
  end

  def valid_credentials?
    @account = Account.find_by_email(@account[:email])
    
    return false unless account
    
    account.password == @account.password
  end
end
