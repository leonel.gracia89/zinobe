class AbstractNotificationContactService
  BASIC_URL = 'https://my-json-server.typicode.com/LGracia/demo'

  def send_notification(contact)
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end
