require_relative 'user'

class Contact < User
  scope :search, ->(param) { where('name LIKE :search OR email LIKE :search', search: "%#{param}%") }

  validates_presence_of :phone

  enum contact_type: [:beneficiario, :padrino]
  enum status: [:uninformed, :notified]
end
