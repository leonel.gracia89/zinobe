class User < ActiveRecord::Base
  self.abstract_class = true

  validates_presence_of :name, :email, :nit
  validates_uniqueness_of :email, :nit
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
end
