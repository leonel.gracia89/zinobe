require_relative 'user'

class Account < User
  has_secure_password

  validates :password, length: { minimum: 3 }

  has_many :contacts
end
