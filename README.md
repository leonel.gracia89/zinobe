# Zinobe Test

Para instalar el proyecto seria necesario entrar en la carpeta del proyecto `cd zinobe` y correr los siguientes comandos:

* `bundle install`
* Crear base de datos `bundle exec rake db:create`
* Correr migraciones `bundle exec rake db:migrate`
* En una terminal aparte es necesario tener corriendo sidekiq, este comando servirá `bundle exec sidekiq -r ./config/sidekiq.rb`
* Para correr el proyecto será necesario éste comando `rackup config.ru`
* Por último, en el navegador se accedera por la siguiente url [http://localhost:9292/](URL "localhost")

### Posibles errores

Probablemente al usar `bundle exec rake db:create` mostrará un error porque no se puede conectar al usuario root, eso se debe a que
no está con la contraseña en blanco, para solucionarlo procedemos a ejecutar los siguientes comandos:

* `sudo mysql -u root`
* Una vez dentro de mysql `USE mysql;`
* `UPDATE user SET plugin='mysql_native_password' WHERE User='root';`
* `FLUSH PRIVILEGES;`
* Salimos de mysql `exit;`
* Reiniciamos el servicio `service mysql restart`
